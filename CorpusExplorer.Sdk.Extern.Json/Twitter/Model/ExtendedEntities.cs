#region

using Newtonsoft.Json;

#endregion

namespace CorpusExplorer.Sdk.Extern.Json.Twitter.Model
{
  public class ExtendedEntities
  {
    [JsonProperty("media")]
    public Media[] Media { get; set; }
  }
}