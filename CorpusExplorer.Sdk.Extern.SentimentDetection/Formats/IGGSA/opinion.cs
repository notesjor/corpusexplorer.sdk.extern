using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace CorpusExplorer.Sdk.Extern.SentimentDetection.Model.IGGSA
{
  /// <remarks />
  [GeneratedCode("xsd", "4.6.1055.0")]
  [Serializable]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(AnonymousType = true)]
  [XmlRoot(Namespace = "", IsNullable = false)]
  public class opinion
  {
    private double polarityField;

    private string sourceField;

    /// <remarks />
    [XmlAttribute]
    public double polarity { get { return polarityField; } set { polarityField = value; } }

    /// <remarks />
    [XmlAttribute]
    public string source { get { return sourceField; } set { sourceField = value; } }
  }
}