﻿#region

using System;
using System.Windows.Forms;
using CorpusExplorer.Sdk.Model;

#endregion

namespace CorpusExplorer.Sdk.View
{
  internal sealed partial class Demo3rdPartyView : UserControl
  {
    private readonly Selection _selection;

    public Demo3rdPartyView(Selection selection)
    {
      _selection = selection;
      InitializeComponent();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      // ReSharper disable LocalizableElement
      MessageBox.Show("Der Schnappschuss enthält: " + _selection.CountDocuments + " Dokumente.");
      // ReSharper restore LocalizableElement
    }
  }
}