﻿#region

using System.Drawing;
using CorpusExplorer.Sdk.Addon;
using CorpusExplorer.Sdk.Model;
using CorpusExplorer.Sdk.Properties;

#endregion

namespace CorpusExplorer.Sdk.View
{
  internal sealed class Demo3rdPartyAddon : IAddonView
  {
    /// <summary>
    ///   Icon das für dieses Addon angezeigt wird (wird nicht von allen Terminals unterstützt).
    ///   Es wird empfohlen ein PNG-Bild mit einer Auflösung von 48*48 Pixeln bereitzustellen.
    /// </summary>
    public Image IconImage { get { return Resources.piece; } }

    /// <summary>
    ///   Name/Bezeichnung des Addons. Achten Sie auf eine möglichst kurze Bezeichnung.
    /// </summary>
    public string Label { get { return "Beispiel ADDON"; } }

    /// <summary>
    ///   Wird aufgerufen wenn die View angezeigt werden soll.
    /// </summary>
    /// <param name="selection">Aktuelles Auswahl auf die das Addon seine Analyse anwenden soll.</param>
    /// <param name="parentContainerControl">
    ///   Übergeordneter Addon-Container. Der Addon-Hersteller muss sicherstellen, dass er
    ///   kompatible GUI-Addon bereitstellt.
    /// </param>
    public void Show(Selection selection, object parentContainerControl)
    {
      var view = new Demo3rdPartyView(selection);
      ((IAddonViewContainer) parentContainerControl).Add(view);
    }
  }
}