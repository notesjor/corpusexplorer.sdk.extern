﻿#region

using System.Collections.Generic;
using CorpusExplorer.Sdk.Addon;
using CorpusExplorer.Sdk.Extern.Binary.Excel.Kidko;
using CorpusExplorer.Sdk.Extern.Binary.Excel.Universal;
using CorpusExplorer.Sdk.Extern.Binary.ListOfScrapDocuments;
using CorpusExplorer.Sdk.Extern.Epub;
using CorpusExplorer.Sdk.Extern.Json.Twitter;
using CorpusExplorer.Sdk.Extern.Json.YourTwapperKeeper;
using CorpusExplorer.Sdk.Extern.NasosoftPdf;
using CorpusExplorer.Sdk.Extern.Plaintext.ClanChildes;
using CorpusExplorer.Sdk.Extern.Plaintext.Csv;
using CorpusExplorer.Sdk.Extern.Plaintext.EasyHashtagSeparation;
using CorpusExplorer.Sdk.Extern.Plaintext.RawMailMsg;
using CorpusExplorer.Sdk.Extern.TextSharp;
using CorpusExplorer.Sdk.Extern.Xml.AnnotationPro;
using CorpusExplorer.Sdk.Extern.Xml.DortmunderChatKorpus;
using CorpusExplorer.Sdk.Extern.Xml.Dpxc;
using CorpusExplorer.Sdk.Extern.Xml.Exmaralda;
using CorpusExplorer.Sdk.Extern.Xml.Folker;
using CorpusExplorer.Sdk.Extern.Xml.Tiger;
using CorpusExplorer.Sdk.Extern.Xml.Wikipedia;
using CorpusExplorer.Sdk.Model.Export.Abstract;
using CorpusExplorer.Sdk.Utils.DocumentProcessing.Importer.Abstract;
using CorpusExplorer.Sdk.Utils.DocumentProcessing.Scraper.Abstract;
using CorpusExplorer.Sdk.Utils.DocumentProcessing.Tagger.Abstract;
using CorpusExplorer.Sdk.View;

#endregion

namespace CorpusExplorer.Sdk
{
  // ReSharper disable once UnusedMember.Global
  public class RepositoryManifest : AbstractAddonRepository
  {
    /// <summary>
    ///   Liste mit Scrapern die lokale Dateien bestehender Korpora importieren (z. B. XML, EXMERaLDA).
    ///   Für Dateien MIT Annotation.
    /// </summary>
    public override IEnumerable<KeyValuePair<string, AbstractImporter>> AddonImporter =>
      new Dictionary<string, AbstractImporter>
      {
        {"CLAN/Childes (*.cex)|*.cex", new ClanChildesImporter()},
        {"SDeWac (sdewac*.tagged)|sdewac*.tagged", new SdewacImporter()}
      };

    /// <summary>
    ///   Liste mit Scrapern die lokale Dateien (z. B. TXT, RTF, DOCX, PDF) in Korpusdokumente konvertieren.
    ///   Für Dateien OHNE Annotation.
    /// </summary>
    public override IEnumerable<KeyValuePair<string, AbstractScraper>> AddonScrapers =>
      new Dictionary<string, AbstractScraper>
      {
        {
          "EasyHashtag Plaintext (*.ehp)|*.ehp",
          new EasyHashtagSeparationScraper()
        },
        {
          "EasyMsg E-Mail-Message (*.msg)|*.msg",
          new RawMsgMsgScraper()
        },
        {
          "CorpusExplorer Rohdaten (*.sdd)|*.sdd",
          new ListOfScrapDocumentScraper()
        },
        {
          "Twitter-JSON via StreamAPI (*.json)|*.json",
          new TwitterScraper()
        },
        {
          "Twitter via yourTwappaKeeper (*.php)|*.php",
          new TwapperScraper()
        },
        {
          "Dortmunder Chat Korpus (*.xml)|*.xml",
          new DortmunderChatKorpusScraper()
        },
        {
          "Mediawiki/Wikipedia-DUMP (*.xml)|*.xml",
          new WikipediaScraper()
        },
        {
          "TiGER-XML (*.xml)|*.xml",
          new TigerScraper()
        },
        {
          "EXMERaLDA-Basic (*.exb)|*.exb",
          new ExmaraldaExbScraper()
        },
        {
          "EPUB-eBook (*.epub)|*.epub",
          new EpubScraper()
        },
        {
          "FOLKER-Transkript (*.flk)|*.flk",
          new FolkerScraper()
        },
        {
          "Universeller Excel™-Scraper (*.xlsx)|*.xlsx",
          new UniversalExcelScraper()
        },
        {
          "KiDKo/E-Scraper (*.xlsx)|*.xlsx",
          new KidkoScraper()
        },
        {
          "AnnotationPro (*.ant)|*.ant",
          new AnnotationProScraper()
        },
        {
          "CSV-Datei mit Überschriften (*.csv)|*.csv",
          new CsvScraper()
        },
        {
          "DocPlusXmlCorpus (*.dpxc)|*.dpxc",
          new DpxcScraper()
        },
        {
          "Nur Text (*.pdf)|*.pdf",
          new NasoPdfScraper()
        },
        {
          "Nur Text (*.pdf - via iTextSharp (simple))|*.pdf",
          new ITextSharpPdfScraperSimple()
        },
        {
          "Nur Text (*.pdf - via iTextSharp (location))|*.pdf",
          new ITextSharpPdfScraperLocation()
        }
      };

    /// <summary>
    ///   Zusätzliche Tagger
    /// </summary>
    public override IEnumerable<AbstractTagger> AddonTagger => null;

    /// <summary>
    ///   Externe Analysemodule.
    /// </summary>
    public override IEnumerable<IAddonView> AddonViews => new[] {new Demo3rdPartyAddon()};

    /// <summary>
    ///   Liste mit Exportern die Projekte, Korpora und Schnappschüsse (alle IHydra) exportieren können
    /// </summary>
    public override IEnumerable<KeyValuePair<string, AbstractExporter>> AddonExporters =>
      new Dictionary<string, AbstractExporter>
      {
        {"DSpin-XML (*.xml)|*.xml", new DSpinExporter()},
        {"AnnotationPro (*.ant)|*.ant", new AnnoationProExporter()}
      };

    /// <summary>
    ///   Eindeutige Bezeichnung (Name) des Addons
    /// </summary>
    public override string Guid => "CorpusExplorer.Sdk.Extern";
  }
}