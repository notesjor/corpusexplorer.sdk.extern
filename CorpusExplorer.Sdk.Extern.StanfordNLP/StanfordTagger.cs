﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CorpusExplorer.Sdk.Helper;
using CorpusExplorer.Sdk.Model;
using CorpusExplorer.Sdk.Utils.DocumentProcessing.Tagger.Abstract;
using edu.stanford.nlp.pipeline;
using edu.stanford.nlp.tagger.maxent;

namespace CorpusExplorer.Sdk.Extern.StanfordNLP
{
  public class StanfordTagger : AbstractTagger
  {
    private Dictionary<string, string> _languages = new Dictionary<string, string>
    {
      {"Arabisch", "arabic.tagger"},
      {"Chinesisch (distsim)", "chinese-distsim.tagger"},
      {"Chinesisch (nodistsim)", "chinese-nodistsim.tagger"},
      {"Englisch (english-bidirectional-distsim)","english-bidirectional-distsim.tagger"},
      {"Englisch (english-caseless-left3words-distsim)","english-caseless-left3words-distsim.tagger"},
      {"Englisch (english-left3words-distsim)","english-left3words-distsim.tagger"},
      {"Englisch (wsj-0-18-bidirectional-distsim)","wsj-0-18-bidirectional-distsim.tagger"},
      {"Englisch (wsj-0-18-bidirectional-nodistsim)","wsj-0-18-bidirectional-nodistsim.tagger"},
      {"Englisch (wsj-0-18-caseless-left3words-distsim)","wsj-0-18-caseless-left3words-distsim.tagger"},
      {"Englisch (wsj-0-18-left3words-distsim)","wsj-0-18-left3words-distsim.tagger"},
      {"Englisch (wsj-0-18-left3words-nodistsim)","wsj-0-18-left3words-nodistsim.tagger"},
      {"Französisch","french.tagger"},
      {"Deutsch (german-dewac)","german-dewac.tagger"},
      {"Deutsch (german-fast)","german-fast.tagger"},
      {"Deutsch (german-fast-caseless)","german-fast-caseless.tagger"},
      {"Deutsch (german-hgc)","german-hgc.tagger"},
    };

    public StanfordTagger()
    {
      InstallationPath = Configuration.GetDependencyPath(@"StanfordPOSTagger\models");
    }

    public override string DisplayName
    {
      get { return "Stanford Maxent POS Tagger"; }
    }

    public override bool IsReady
    {
      get
      {
        return Directory.Exists(InstallationPath) && this.ScraperResults != null && this.ScraperResults.Any();
      }
    }

    public override string InstallationPath { get; set; }

    public override IEnumerable<string> LangaugeAvailabel
    {
      get { return _languages.Select(x => x.Key); }
    }

    public override string LanguageSelected { get; set; }

    public string CorpusDisplayname { get; set; }

    public override Corpus Execute()
    {
      var modelPath = Path.Combine(InstallationPath, _languages[LanguageSelected]);
      if (!File.Exists(modelPath)) return null;

      var tagger = new MaxentTagger(modelPath);

      foreach (var document in ScraperResults)
      {
        
      }

      return Corpus.Create(CorpusDisplayname, );
    }
  }
}
