﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CorpusExplorer.Core.DocumentProcessing.Tagger.TreeTagger;
using CorpusExplorer.Export2.AnnotationPro.Properties;
using CorpusExplorer.Sdk.Ecosystem.Model;
using CorpusExplorer.Sdk.Extern.Xml.AnnotationPro;
using CorpusExplorer.Sdk.Model.Adapter.Corpus;

namespace CorpusExplorer.Export2.AnnotationPro
{
  internal sealed partial class Form1 : Form
  {
    private readonly Dictionary<string, string> _languages = new Dictionary<string, string>
    {
      {"Deutsch", Resources.DisplayLanguage_German},
      {"Englisch", Resources.DisplayLanguage_English},
      {"Französisch", Resources.DisplayLanguage_French},
      {"Italienisch", Resources.DisplayLanguage_Italian},
      {"Niederländisch", Resources.DisplayLanguage_Dutch},
      {"Spanisch", Resources.DisplayLanguage_Spanish},
      {"Polnisch", Resources.DisplayLanguage_Polish}
    };

    public Form1() { InitializeComponent(); }

    private void btn_to_apro_Click(object sender, EventArgs e)
    {
      var ofd1 = new OpenFileDialog
      {
        CheckFileExists = true,
        Filter = Resources.FileExtension_CEC5,
        Multiselect = false
      };

      if (ofd1.ShowDialog() != DialogResult.OK)
        return;

      MessageBox.Show(
        Resources.Form1_SelectAntFile);

      var ofd2 = new OpenFileDialog
      {
        CheckFileExists = true,
        Filter = Resources.FileExtension_ANT,
        Multiselect = false
      };

      if (ofd2.ShowDialog() != DialogResult.OK)
        return;

      var exporter = new AnnoationProExporter();
      exporter.Export(CorpusAdapterSingleFile.Create(ofd1.FileName), ofd2.FileName);

      MessageBox.Show(Resources.Form1_ConvertToAntDone);
    }

    private void btn_to_ce_Click(object sender, EventArgs e)
    {
      MessageBox.Show(
        Resources.Form1_YouNeedATextLayer);

      var ofd = new OpenFileDialog
      {
        CheckFileExists = true,
        Filter = Resources.FileExtension_ANT,
        Multiselect = false
      };

      if (ofd.ShowDialog() != DialogResult.OK)
        return;

      var scraper = new AnnotationProScraper();
      scraper.Input.Enqueue(ofd.FileName);
      scraper.Execute();

      var tagger = new ClassicTreeTagger
      {
        Input = scraper.Output,
        LanguageSelected = comboBox1.SelectedValue.ToString()
      };

      var sfd = new SaveFileDialog
      {
        CheckPathExists = true,
        Filter = Resources.FileExtension_CEC5
      };
      if (sfd.ShowDialog() != DialogResult.OK)
        return;

      tagger.Execute();
      tagger.Output.FirstOrDefault()?.Save(sfd.FileName, true);

      MessageBox.Show(Resources.Form1_ConvertToCec5Done);
    }

    private void Form1_Load(object sender, EventArgs e)
    {
      Configuration.Initialize();

      comboBox1.DisplayMember = "Value";
      comboBox1.ValueMember = "Key";
      comboBox1.DataSource = _languages.ToList();
      comboBox1.AutoCompleteMode = AutoCompleteMode.Append;
    }
  }
}