using CorpusExplorer.Export2.AnnotationPro.Properties;
namespace CorpusExplorer.Export2.AnnotationPro
{
  internal sealed partial class Form1
  {
    /// <summary>
    /// Erforderliche Designervariable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Verwendete Ressourcen bereinigen.
    /// </summary>
    /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Vom Windows Form-Designer generierter Code

    /// <summary>
    /// Erforderliche Methode für die Designerunterstützung.
    /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.comboBox1 = new System.Windows.Forms.ComboBox();
      this.label3 = new System.Windows.Forms.Label();
      this.btn_to_ce = new System.Windows.Forms.Button();
      this.btn_to_apro = new System.Windows.Forms.Button();
      this.pictureBox2 = new System.Windows.Forms.PictureBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.label4 = new System.Windows.Forms.Label();
      this.groupBox1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
      this.groupBox2.SuspendLayout();
      this.SuspendLayout();
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.comboBox1);
      this.groupBox1.Controls.Add(this.label3);
      this.groupBox1.Controls.Add(this.btn_to_ce);
      this.groupBox1.Location = new System.Drawing.Point(17, 106);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(275, 131);
      this.groupBox1.TabIndex = 4;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Ich habe eine ANT-Datei";
      // 
      // comboBox1
      // 
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Location = new System.Drawing.Point(14, 50);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new System.Drawing.Size(240, 28);
      this.comboBox1.TabIndex = 3;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(14, 26);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(140, 20);
      this.label3.TabIndex = 2;
      this.label3.Text = "ANT-Datei Sprache:";
      // 
      // btn_to_ce
      // 
      this.btn_to_ce.Font = new System.Drawing.Font("Segoe UI", 10F);
      this.btn_to_ce.Location = new System.Drawing.Point(14, 85);
      this.btn_to_ce.Name = "btn_to_ce";
      this.btn_to_ce.Size = new System.Drawing.Size(240, 36);
      this.btn_to_ce.TabIndex = 0;
      this.btn_to_ce.Text = global::CorpusExplorer.Export2.AnnotationPro.Properties.Resources.Form1_ConvertFromAntToCec5;
      this.btn_to_ce.UseVisualStyleBackColor = true;
      this.btn_to_ce.Click += new System.EventHandler(this.btn_to_ce_Click);
      // 
      // btn_to_apro
      // 
      this.btn_to_apro.Font = new System.Drawing.Font("Segoe UI", 10F);
      this.btn_to_apro.Location = new System.Drawing.Point(17, 34);
      this.btn_to_apro.Name = "btn_to_apro";
      this.btn_to_apro.Size = new System.Drawing.Size(240, 36);
      this.btn_to_apro.TabIndex = 1;
      this.btn_to_apro.Text = global::CorpusExplorer.Export2.AnnotationPro.Properties.Resources.Form1_ConvertFromCec5ToAnt;
      this.btn_to_apro.UseVisualStyleBackColor = true;
      this.btn_to_apro.Click += new System.EventHandler(this.btn_to_apro_Click);
      // 
      // pictureBox2
      // 
      this.pictureBox2.Image = global::CorpusExplorer.Export2.AnnotationPro.Properties.Resources.export;
      this.pictureBox2.Location = new System.Drawing.Point(35, 23);
      this.pictureBox2.Name = "pictureBox2";
      this.pictureBox2.Size = new System.Drawing.Size(64, 64);
      this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.pictureBox2.TabIndex = 2;
      this.pictureBox2.TabStop = false;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Segoe UI Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(106, 23);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(352, 25);
      this.label1.TabIndex = 5;
      this.label1.Text = "AnnoationPro & CorpusExplorer Konverter";
      this.label1.UseMnemonic = false;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Segoe UI", 9F);
      this.label2.Location = new System.Drawing.Point(111, 52);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(489, 30);
      this.label2.TabIndex = 6;
      this.label2.Text = "Konvertiert den Layer \"Text\" aus AnnoationPro als CorpusExplorer Korpus.\r\nNutzen " +
    "Sie den CorpusExplorer und konvertieren Sie die Ergebnisse zurück in AnnotaionPr" +
    "o.";
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.label4);
      this.groupBox2.Controls.Add(this.btn_to_apro);
      this.groupBox2.Location = new System.Drawing.Point(298, 106);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(275, 131);
      this.groupBox2.TabIndex = 7;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "ICH habe eine CEC5-Datei";
      // 
      // label4
      // 
      this.label4.Font = new System.Drawing.Font("Segoe UI", 9F);
      this.label4.Location = new System.Drawing.Point(22, 81);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(235, 47);
      this.label4.TabIndex = 8;
      this.label4.Text = "Achtung: Wenn Sie eine Änderung in der ANT-Datei\r\nvornehmen kann das reimportiere" +
    "n diese Änderung verwerfen.";
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.White;
      this.ClientSize = new System.Drawing.Size(607, 249);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.pictureBox2);
      this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.Name = "Form1";
      this.Text = "Konvertiere AnnotationPro zu CorpusExplorer und vice versa";
      this.Load += new System.EventHandler(this.Form1_Load);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
      this.groupBox2.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.PictureBox pictureBox2;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Button btn_to_ce;
    private System.Windows.Forms.Button btn_to_apro;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.ComboBox comboBox1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Label label4;
  }
}

