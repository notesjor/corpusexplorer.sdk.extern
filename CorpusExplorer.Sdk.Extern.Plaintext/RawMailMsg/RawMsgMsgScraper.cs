﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using CorpusExplorer.Sdk.Extern.Plaintext.Abstract;
using MsgReader;

#endregion

namespace CorpusExplorer.Sdk.Extern.Plaintext.RawMailMsg
{
  public sealed class RawMsgMsgScraper : AbstractPlaintextScraper
  {
    private string _tempPath;

    public override string DisplayName { get { return "Easy MSG E-Mail"; } }

    public override void Execute()
    {
      _tempPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N"));
      Directory.CreateDirectory(_tempPath);

      // Optimierung muss abgeschaltet werden, da sonst Threadsprünge möglich sind, die nicht erwünscht sind.
      // Konkret: Der Ordner _tempPath kann gelöscht werden bevor alle Nachrichten gelesen wurden.
      // ReSharper disable LoopCanBeConvertedToQuery
      foreach (var file in Input)
      {
        var temp = Execute(file);
        if (temp == null)
          continue;
        foreach (var x in temp)
          Output.Enqueue(x);
      }
      // ReSharper restore LoopCanBeConvertedToQuery

      Directory.Delete(_tempPath, true);
    }

    protected override IEnumerable<Dictionary<string, object>> Execute(string file)
    {
      var reader = new Reader();

      var lines = reader.ExtractToFolder(file, _tempPath); // File.ReadLines(file);
      var dic = new Dictionary<string, object>();

      foreach (var line in lines)
      {
        GetValue(line, ref dic, "Message-ID: ", "MessageID");
        GetValue(line, ref dic, "Date:  ", "Datum");
        GetValue(line, ref dic, "Subject: ", "Titel");
        GetValue(line, ref dic, "From: ", "Absender");
        GetValue(line, ref dic, "To: ", "Empfänger");
      }

      return new[] {dic};
    }

    private static void GetValue(string line, ref Dictionary<string, object> dic, string query, string key)
    {
      if (line.StartsWith(query) &&
          !dic.ContainsKey(key))
        dic.Add(key, line.Substring(query.Length));
    }
  }
}