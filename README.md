# Inforamtionen zu CorpusExplorer.Sdk.Extern #

Dieses Projekt ist Teil des *Software Development Kit - des CorpusExplorers* (CorpusExplorer SDK) [[Weitere Informationen finden Sie hier]](http://notes.jan-oliver-ruediger.de/corpusexplorer/sdk/). Das SDK sowie alle Teile können kostenlos für Forschungs- und Bildungsprojekte genutzt werden. Dieser Teil des SDK steht unter der [GPL-3.0-Lizenz](http://www.gnu.de/documents/gpl-3.0.de.html). Sie können dieses Projekt nutzen um:

* Den CorpusExplorer zu erweitern.

* Ihr eigenes Programm mit dem CorpusExplorer zu verbinden (API-Schnittstelle).

* Oder unabhängig vom CorpusExplorer ihr eigenes Programm zu entwickeln/erweitern und so auf bewährte Lösungen zurückzugreifen.

### Kurzinformationen ###

* SDK zum Lesen/Schreiben von Binär/XML-Datenformaten die von vielen korpuslinguistischen Projekten verwendet werden.
* Version: 1.0
* [Informationen zum gesamten SDK finden Sie hier.](http://notes.jan-oliver-ruediger.de/corpusexplorer/sdk/)
* [Informationen zum CorpusExplorer finden Sie hier.](http://notes.jan-oliver-ruediger.de/corpusexplorer/)

### Wie kompiliere ich den Code selbst? ###

* Zuerst sollte das Repo mittels GIT geklont werden. [[Anleitung]](http://gitref.org/creating/#clone)
* Sie benötigen Visual Studio 2013. [[Visual Studio 2013 - Desktop Express (kostenlos)]](http://www.microsoft.com/de-de/download/details.aspx?id=40787)
* Die Assemblies lassen sich so problemlos kompilieren.
* Sollten Sie die Tests kompilieren wollen, so müssen Sie zuerst entsprechendes Korpusmaterial besorgen. Entsprechendes DEMO-Korpusmaterial oder die jeweiligen Originalkorpora erhalten Sie über die Webseiten der jeweiligen Anbieter (z. B. [[EXMARaLDA Demokorpus]](http://www.exmaralda.org/corpora/demokorpus.html), [[FOLKER-Korpus]](http://agd.ids-mannheim.de/folker_download.shtml), usw.) - Wenn Sie das Testprojekt laden, sehen Sie welche Dateien im Ordner (testdata) fehlen. Die Dateinamen entsprechen exakt den Originalkorpora.

### Wie ist das Projekt strukturiert? ###

* CorpusExplorer.Sdk.Extern: Dieses Projekt zeigt die Schnittstellen zwischen CorpusExplorer und den Externen-Formaten und wie diese für den CorpusExplorer aufbereitet werden. Sie können das Projekt als Beispiel für eigene Implementierungen verwenden (z. B. als Beispiel für einen eigenen Konverter). Sie benötigen das Projekt ebenfalls, wenn Sie die API des CorpusExplorers nutzen wollen.
* CorpusExplorer.Sdk.Extern.Test: Testprojekt zu CorpusExplorer.Sdk.Extern
* CorpusExplorer.Sdk.Extern.Binary: Das Projekt stellt Schnittstellen (API) für Binäre-Dateiformate bereit, z. B. Korpora in Microsoft-Excel-Tabellen.
* CorpusExplorer.Sdk.Extern.Binary.Test: Testprojekt zu CorpusExplorer.Sdk.Extern.Binary
* CorpusExplorer.Sdk.Extern.Xml: Das Projekt stellt Schnittstellen (API) für XML-Dateien bereit und mappt diese direkt auf entsprechende .NET-Objekte. Dadurch lassen sich die verfügbaren XML-Dateien direkt in C#/VB.NET/etc. de-/serialisieren.
* CorpusExplorer.Sdk.Extern.Xml.Test: Testprojekt zu CorpusExplorer.Sdk.Extern.Xml

### Wer ist Ansprechpartner? ###

* [Kontaktdaten finden Sie hier.](http://notes.jan-oliver-ruediger.de/impressum/)