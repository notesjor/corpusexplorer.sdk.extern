﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CorpusExplorer.Sdk.Utils.DocumentProcessing.Scraper.Abstract;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

namespace CorpusExplorer.Sdk.Extern.TextSharp
{
  public class ITextSharpPdfScraperSimple : AbstractScraper
  {
    public override string DisplayName => "iTextSharp (simple)";

    protected override IEnumerable<Dictionary<string, object>> Execute(string file)
    {
      using (var fs = new FileStream(file, FileMode.Open, FileAccess.Read))
      {
        var pdfReader = new PdfReader(fs);
        var stb = new StringBuilder();

        var strategy = new SimpleTextExtractionStrategy();
        for (var i = 0; i < pdfReader.NumberOfPages; i++)
          stb.AppendLine(PdfTextExtractor.GetTextFromPage(pdfReader, i + 1, strategy));

        var res = new Dictionary<string, object> {{"Text", stb.ToString()}};
        foreach (var info in pdfReader.Info.Where(info => !res.ContainsKey(info.Key)))
          res.Add(info.Key, info.Value);

        return new[] {res};
      }
    }
  }
}