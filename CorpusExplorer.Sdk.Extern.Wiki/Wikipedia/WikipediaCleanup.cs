﻿using System.IO;
using CorpusExplorer.Sdk.Ecosystem.Model;
using CorpusExplorer.Sdk.Utils.DocumentProcessing.Cleanup.Abstract;
using Telerik.Windows.Documents.Flow.FormatProviders.Html;
using Telerik.Windows.Documents.Flow.FormatProviders.Txt;
using Telerik.Windows.Documents.Flow.Model;
using WikiPlex;

namespace CorpusExplorer.Sdk.Extern.Wiki.Wikipedia
{
  public class WikipediaCleanup : AbstractCleanup
  {
    private readonly WikiEngine _engine = new WikiEngine();
    private readonly TxtFormatProvider _provider = new TxtFormatProvider();
    public override string DisplayName => "Wikipedia Cleanup";

    protected override string Execute(string key, string input) { return ExecuteInline(input); }

    public string ExecuteInline(string raw)
    {
      RadFlowDocument doc;
      var reader = new HtmlFormatProvider {ImportSettings = new HtmlImportSettings {ReplaceNonBreakingSpaces = true}};
      using (var ms = new MemoryStream(Configuration.Encoding.GetBytes(_engine.Render(raw))))
      {
        doc = reader.Import(ms);
      }

      string text;
      using (var ms = new MemoryStream())
      {
        _provider.Export(doc, ms);
        ms.Seek(0, SeekOrigin.Begin);
        var buffer = new byte[ms.Length];
        ms.Read(buffer, 0, buffer.Length);
        text = Configuration.Encoding.GetString(buffer);
      }

      return text;
    }
  }
}