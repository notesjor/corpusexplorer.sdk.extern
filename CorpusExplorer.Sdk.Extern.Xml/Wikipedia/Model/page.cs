using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

namespace CorpusExplorer.Sdk.Extern.Xml.Wikipedia.Model
{
  /// <remarks />
  [GeneratedCode("xsd", "4.0.30319.33440")]
  [Serializable]
  [DebuggerStepThrough]
  [DesignerCategory("code")]
  [XmlType(AnonymousType = true)]
  [XmlRoot("page", Namespace = "", IsNullable = false)]
  public class page
  {
    private string idField;
    private string nsField;
    private redirect redirectField;
    private string restrictionsField;
    private revision revisionField;
    private string titleField;

    /// <remarks />
    public string title { get { return titleField; } set { titleField = value; } }

    /// <remarks />
    [XmlElement(DataType = "integer")]
    public string ns { get { return nsField; } set { nsField = value; } }

    /// <remarks />
    [XmlElement(DataType = "integer")]
    public string id { get { return idField; } set { idField = value; } }

    /// <remarks />
    public redirect redirect { get { return redirectField; } set { redirectField = value; } }

    /// <remarks />
    public string restrictions { get { return restrictionsField; } set { restrictionsField = value; } }

    /// <remarks />
    public revision revision { get { return revisionField; } set { revisionField = value; } }
  }
}