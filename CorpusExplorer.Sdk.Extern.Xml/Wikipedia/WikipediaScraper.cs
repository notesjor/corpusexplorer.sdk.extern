﻿#region

using System;
using System.Collections.Generic;
using CorpusExplorer.Sdk.Extern.Xml.Abstract;
using CorpusExplorer.Sdk.Extern.Xml.Wikipedia.Model;
using CorpusExplorer.Sdk.Extern.Xml.Wikipedia.Serializer;
using CorpusExplorer.Sdk.Helper;

#endregion

namespace CorpusExplorer.Sdk.Extern.Xml.Wikipedia
{
  public sealed class WikipediaScraper : AbstractGenericXmlFormatScraper<page>
  {
    public override string DisplayName { get { return "Mediawiki / Wikipedia"; } }

    protected override AbstractGenericSerializer<page> Serializer { get { return new WikipediaFullDumpSerializer(); } }

    public IEnumerable<Dictionary<string, object>> ScrapDocumentsInline(page model) { return ScrapDocuments(model); }

    protected override IEnumerable<Dictionary<string, object>> ScrapDocuments(page model)
    {
      try
      {
        if (model.redirect == null && model.revision != null)
        {
          var rev = model.revision;
          if (rev == null || rev.text == null || rev.text.Text == null)
            return null;

          var doc = new Dictionary<string, object>
                    {
                      // Eigenschaften der Seite
                      {"Id", model.id},
                      {"Titel", model.title},
                      {"Text", string.Join(" ", rev.text.Text)},
                      {"Zeitstempel", rev.timestamp}
                    };
          return new[] {new Dictionary<string, object>(doc)};
        }
      }
      catch (Exception ex) {
        InMemoryErrorConsole.Log(ex);
      }
      return null;
    }
  }
}