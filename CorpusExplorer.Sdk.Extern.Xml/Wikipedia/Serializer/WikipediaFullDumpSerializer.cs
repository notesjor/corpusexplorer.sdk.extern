﻿#region

using CorpusExplorer.Sdk.Extern.Xml.Abstract;
using CorpusExplorer.Sdk.Extern.Xml.Wikipedia.Model;

#endregion

namespace CorpusExplorer.Sdk.Extern.Xml.Wikipedia.Serializer
{
  // ReSharper disable once UnusedMember.Global
  public class WikipediaFullDumpSerializer : AbstractGenericSerializer<page>
  {
    protected override void DeserializePostValidation(page obj, string path) { }

    protected override void DeserializePreValidation(string path) { CheckFileExtension(path, "xml"); }

    protected override void SerializePostValidation(page obj, string path) { }

    protected override void SerializePreValidation(page obj, string path) { CheckFileExtension(path, "xml"); }
  }
}