﻿#region

using System.Data;
using System.IO;
using Excel;

#endregion

namespace CorpusExplorer.Sdk.Extern.Binary.Excel.Universal.Helper
{
  internal static class ExcelUniversalDataSetHelper
  {
    public static DataSet GetDataSet(string path, bool isFirstRowAsColumnNames = true)
    {
      using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read))
      {
        var reader = path.EndsWith(".xlsx")
                       ? ExcelReaderFactory.CreateOpenXmlReader(fs)
                       : ExcelReaderFactory.CreateBinaryReader(fs);

        reader.IsFirstRowAsColumnNames = isFirstRowAsColumnNames;
        return reader.AsDataSet(true);
      }
    }
  }
}