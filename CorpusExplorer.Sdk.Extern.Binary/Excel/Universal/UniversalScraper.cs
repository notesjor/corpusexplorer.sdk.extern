﻿namespace CorpusExplorer.Sdk.Extern.Binary.Excel.Universal
{
  using System.Collections.Generic;
  using System.Linq;

  using CorpusExplorer.Sdk.Extern.Binary.Abstract;
  using CorpusExplorer.Sdk.Extern.Binary.Excel.Universal.Reader;
  using CorpusExplorer.Sdk.Model.Scraper;

  public class UniversalScraper : AbstractGenericBinaryFormatScraper<Dictionary<string, string>>
  {
    private readonly IEnumerable<string> _columnHeaders;

    private readonly int _tableIndex;

    public UniversalScraper(IEnumerable<string> columnHeaders, int tableIndex = 0)
    {
      this._columnHeaders = columnHeaders;
      this._tableIndex = tableIndex;
    }

    protected override IEnumerable<ScrapDocument> ScrapDocuments(IEnumerable<Dictionary<string, string>> model)
    {
      return model.Select(m => new ScrapDocument(m.ToDictionary<KeyValuePair<string, string>, string, object>(x => x.Key, x => x.Value)));
    }

    public override AbstractGenericDataReader<Dictionary<string, string>> DataReader
    {
      get
      {
        return new ExcelUniversalDataReader(this._columnHeaders, this._tableIndex);
      }
    }

    public override string DisplayName
    {
      get
      {
        return "Universeller Excel™-Scraper";
      }
    }
  }
}
